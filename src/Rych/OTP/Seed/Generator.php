<?php
/**
 * RFC 4226 OTP Library
 *
 * @package Rych\OTP
 * @author Ryan Chouinard <rchouinard@gmail.com>
 * @copyright Copyright (c) 2012, Ryan Chouinard
 * @license MIT License - http://www.opensource.org/licenses/mit-license.php
 */

namespace Rych\OTP\Seed;

use Rych\OTP\Seed\Encoder as EncoderInterface;
use Rych\OTP\Seed\Random as RandomInterface;

/**
 * Seed generator interface
 *
 * @package Rych\OTP\SeedGenerator
 */
interface Generator
{

    /**
     * @param EncoderInterface $encoder
     * @return self
     */
    public function setEncoder(EncoderInterface $encoder);

    /**
     * @return EncoderInterface
     */
    public function getEncoder();

    /**
     * @param RandomInterface $random
     * @return self
     */
    public function setRandom(RandomInterface $random);

    /**
     * @return RandomInterface
     */
    public function getRandom();


    /**
     * @param integer $bytes
     * @return string
     */
    public function generate($bytes);

}
