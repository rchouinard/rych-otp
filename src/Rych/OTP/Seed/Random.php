<?php
/**
 * RFC 4226 OTP Library
 *
 * @package Rych\OTP
 * @author Ryan Chouinard <rchouinard@gmail.com>
 * @copyright Copyright (c) 2012, Ryan Chouinard
 * @license MIT License - http://www.opensource.org/licenses/mit-license.php
 */

namespace Rych\OTP\Seed;

/**
 * Seed random interface
 *
 * @package Rych\OTP\SeedRandom
 */
interface Random
{

    /**
     * @param integer $bytes
     * @return string
     */
    public function generate($bytes);

}
