<?php
/**
 * RFC 4226 OTP Library
 *
 * @package Rych\OTP
 * @author Ryan Chouinard <rchouinard@gmail.com>
 * @copyright Copyright (c) 2012, Ryan Chouinard
 * @license MIT License - http://www.opensource.org/licenses/mit-license.php
 */

namespace Rych\OTP\Seed\Random;

use Rych\OTP\Seed\Random as RandomInterface;
use Rych\Random\Generator as RychGenerator;

/**
 * Rych\Random source
 *
 * @package Rych\OTP\SeedRandom
 */
class RychRandom extends RychGenerator implements RandomInterface
{
}
