<?php
/**
 * RFC 4226 OTP Library
 *
 * @package Rych\OTP
 * @author Ryan Chouinard <rchouinard@gmail.com>
 * @copyright Copyright (c) 2012, Ryan Chouinard
 * @license MIT License - http://www.opensource.org/licenses/mit-license.php
 */

namespace Rych\OTP\Seed\Generator;

use Rych\OTP\Seed\Encoder as EncoderInterface;
use Rych\OTP\Seed\Generator as GeneratorInterface;
use Rych\OTP\Seed\Random as RandomInterface;
use Rych\OTP\Seed\Encoder\Base32 as Base32Encoder;
use Rych\OTP\Seed\Random\RychRandom as RychRandomGenerator;

/**
 * Standard seed generator
 *
 * @package Rych\OTP\SeedGenerator
 */
class Standard implements GeneratorInterface
{

    /**
     * @var EncoderInterface
     */
    private $encoder;

    /**
     * @var RandomInterface
     */
    private $random;

    /**
     * @return void
     */
    public function __construct()
    {
        $this->setEncoder(new Base32Encoder);
        $this->setRandom(new RychRandomGenerator);
    }

    /**
     * @param EncoderInterface $encoder
     * @return self
     */
    public function setEncoder(EncoderInterface $encoder)
    {
        $this->encoder = $encoder;
        return $this;
    }

    /**
     * @return EncoderInterface
     */
    public function getEncoder()
    {
        return $this->encoder;
    }

    /**
     * @param RandomInterface $random
     * @return self
     */
    public function setRandom(RandomInterface $random)
    {
        $this->random = $random;
        return $this;
    }

    /**
     * @return RandomInterface
     */
    public function getRandom()
    {
        return $this->random;
    }

    /**
     * @param integer $bytes
     * @return string
     */
    public function generate($bytes = 20)
    {
        return $this->encoder->encode($this->random->generate($bytes));
    }

}
