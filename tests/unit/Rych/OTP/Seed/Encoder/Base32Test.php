<?php

namespace Rych\OTP\Seed\Encoder;

class Base32Test extends \PHPUnit_Framework_TestCase
{

    private $encoder;

    public function setUp()
    {
        $this->encoder = new Base32;
    }

    /**
     * @return array
     */
    public function vectorProvider()
    {
        return array (
            array ('', ''),
            array ('AB', 'IFBA===='),
            array ('f', 'MY======'),
            array ('fo', 'MZXQ===='),
            array ('foo', 'MZXW6==='),
            array ('foob', 'MZXW6YQ='),
            array ('fooba', 'MZXW6YTB'),
            array ('foobar', 'MZXW6YTBOI======'),
            array ("\x95\x46\x8A\x88\xA0\x26\x3B\x60", 'SVDIVCFAEY5WA==='),
        );
    }

    /**
     * @dataProvider vectorProvider()
     */
    public function testEncode($decoded, $encoded)
    {
        $this->assertEquals($encoded, $this->encoder->encode($decoded));
    }

    /**
     * @dataProvider vectorProvider()
     */
    public function testDecode($decoded, $encoded)
    {
        $this->assertEquals($decoded, $this->encoder->decode($encoded));
    }

}
