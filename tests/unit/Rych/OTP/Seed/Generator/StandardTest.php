<?php

namespace Rych\OTP\Seed\Generator;

use Rych\OTP\Seed\Random\RychRandom as RandomGenerator;
use Rych\Random\Source\Mock as MockSource;

class StandardTest extends \PHPUnit_Framework_TestCase
{

    private $generator;

    private $rawSeed = "\xB7\xCC\x85\xD2\x80\x52\x20\x3C";

    public function setUp()
    {
        $randomGenerator = new RandomGenerator;
        $randomGenerator->setSource(new MockSource($this->rawSeed));

        $this->generator = new Standard;
        $this->generator->setRandom($randomGenerator);
    }

    public function testGenerate()
    {
        $this->assertEquals('W7GILUUAKIQDY===', $this->generator->generate(8));
        $this->assertEquals('W7GILUUAKIQDZN6MQXJIAURAHS34ZBOS', $this->generator->generate(20));

        // Check that changing the static seed value affects the output
        // of the generate() method.
        $data = $this->generator->getRandom()->getSource()->getData();
        $data = strrev($data);
        $this->generator->getRandom()->getSource()->setData($data);

        $this->assertEquals('HQQFFAGSQXGLO===', $this->generator->generate(8));
        $this->assertEquals('HQQFFAGSQXGLOPBAKKANFBOMW46CAUUA', $this->generator->generate(20));
    }

}
