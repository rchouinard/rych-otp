<?php

namespace Rych\Random\Source;

use Rych\Random\Source;

class Mock implements Source
{

    private $data;

    public function __construct($data = null) {
        $this->setData($data);
    }

    public function setData($data) {
        $this->data = (string) $data;
        return $this;
    }

    public function getData()
    {
        return (string) $this->data;
    }

    public function read($bytes)
    {
        $data = $this->getData();

        while (strlen($data) < $bytes) {
            $data .= $data;
        }

        return substr($data, 0, $bytes);
    }

}
